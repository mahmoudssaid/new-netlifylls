import { Component, OnInit } from '@angular/core';
import { UserDataService } from 'src/app/services/user-data.service';
@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss'],
})
export class UserComponent implements OnInit {
  charSear: string = '';

  constructor(private _UserDataService: UserDataService) {
    _UserDataService.checkEdit = false;
  }

  Users: [] = [];

  ngOnInit(): void {
    this._UserDataService.getUsers().subscribe((res) => {
      this.Users = res.data;
    });
  }

  editUser(user: any) {
    this._UserDataService.editUserS(user);
    this._UserDataService.checkEdit = true;
  }
  file: any;

  handleFileInput(event: any) {
    const eve = event.files;
    this.file = eve[0];
    console.log(this.file);
  }
  getUsers() {
    this._UserDataService.GetUsersFromFile(this.file).subscribe((res) => {
      console.log(res);
    });
  }
}
