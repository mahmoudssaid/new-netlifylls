import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UserDataService } from 'src/app/services/user-data.service';

@Component({
  selector: 'app-add-or-edit-user',
  templateUrl: './add-or-edit-user.component.html',
  styleUrls: ['./add-or-edit-user.component.scss'],
})
export class AddOrEditUserComponent implements OnInit {
  /* --------------------------- name of form group --------------------------- */
  addUser = new FormGroup({
    id: new FormControl(),
    password: new FormControl(),
    firstname: new FormControl(null, [
      Validators.required,
      Validators.minLength(3),
      Validators.maxLength(10),
    ]),
    lastname: new FormControl(null, [
      Validators.required,
      Validators.minLength(3),
      Validators.maxLength(10),
    ]),
    username: new FormControl(null, [Validators.required]),
    profileImg: new FormControl(null),
    gender: new FormControl('', [Validators.required]),
    email: new FormControl(null, [Validators.required, Validators.email]),
    roleId: new FormControl(null),
  });

  constructor(
    private _Router: Router,
    public _UserDataService: UserDataService,
    private _ActivatedRoute: ActivatedRoute
  ) {}

  btnName: string = 'Add';
  Users1: any[] = [];
  userEdit: any[] = [];
  /* ------------------------------ submit form ------------------------------ */
  subObject(addUser: FormGroup): void {
    if (this.checkEdit) {
      this._UserDataService.sendEditUser(addUser).subscribe((res) => {});
    } else {
      this._UserDataService.sendUser(addUser).subscribe((res) => {});
    }
    this.addUser.reset();
    this._Router.navigate(['/control/users']);
  }

  userID: string = '';

  deleteUser() {
    this._UserDataService.deleteUser(this.userID).subscribe((res) => {});
    this.addUser.reset();
    this._Router.navigate(['/control/users']);
    this.checkEdit = false;
  }
  checkEdit = this._UserDataService.checkEdit;
  indexNum = this._UserDataService.indexNum;

  unSub: any;

  ngOnInit(): void {
    /* ----------------- function to get data from API ----------------- */
    if (this.checkEdit) {
      this.btnName = 'Edit';
      this._UserDataService.dataUser;
      this.unSub = this._UserDataService.dataUser.subscribe((res: any) => {
        this.userID = res.id;
        this.addUser.patchValue({
          id: res.id,
          password: res.password,
          firstname: res.firstName,
          lastname: res.lastName,
          username: res.username,
          profileImg: res.profileImg,
          gender: res.gender,
          email: res.email,
          role: res.role,
        });
      });
    }
  }

  CanDeactivate() {
    if (this.addUser.status == 'VALID' && this.addUser.touched) {
      return window.confirm('your data not save');
    } else {
      return true;
    }
  }

  ngOnDestroy(): void {
    if (this.unSub) {
      this.unSub.unsubscribe();
      this.checkEdit = false;
    }
    this._UserDataService.stop = false;
  }
}
