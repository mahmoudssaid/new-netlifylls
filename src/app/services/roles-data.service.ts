import { BehaviorSubject, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root',
})
export class RolesDataService {
  constructor(private _HttpClient: HttpClient) {}

  api: string = 'https://asrt-lls-api.herokuapp.com/api';

  getRoles(): Observable<any> {
    const headers = { 'Content-Type': 'application/json' };
    return this._HttpClient.post(`${this.api}/role/GetAllRoles`, '{}', {
      headers,
    });
  }

  AddRole(form: any): Observable<any> {
    const headers = { 'Content-Type': 'application/json' };
    return this._HttpClient.post(`${this.api}/role/Create`, form, { headers });
  }

  EditRole(form: any): Observable<any> {
    const headers = { 'Content-Type': 'application/json' };
    return this._HttpClient.post(`${this.api}/role/Edit`, form, { headers });
  }

  DeleteRole(form: any): Observable<any> {
    const headers = { 'Content-Type': 'application/json' };
    return this._HttpClient.post(`${this.api}/role/Delete`, form, { headers });
  }

  getRoleById(id: any): Observable<any> {
    const headers = { 'Content-Type': 'application/json' };
    return this._HttpClient.post(
      `${this.api}/role/GetRoleWithPermissions`,
      { id },
      { headers }
    );
  }
}
