import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class UserDataService {
  /* ----------------------- transfer DataUser to Edit ----------------------- */
  dataUser: any = new BehaviorSubject(null);

  checkEdit: boolean = false;

  indexNum: number = 0;

  stop: any = false;

  editUserS(user: any) {
    this.dataUser.next(user);
  }
  constructor(private _HttpClient: HttpClient) {}

  api: string = 'https://asrt-lls-api.herokuapp.com/api';

  getUsers(): Observable<any> {
    const headers = { 'Content-Type': 'application/json' };
    return this._HttpClient.post(`${this.api}/user/GetUsers`, '{}', {
      headers,
    });
  }

  sendUser(form: any): Observable<any> {
    const headers = { 'Content-Type': 'application/json' };
    return this._HttpClient.post(`${this.api}/user/Create`, form, {
      headers,
    });
  }

  sendEditUser(form: any): Observable<any> {
    const headers = { 'Content-Type': 'application/json' };
    return this._HttpClient.post(`${this.api}/user/Edit`, form, {
      headers,
    });
  }

  deleteUser(id: string) {
    const headers = { 'Content-Type': 'application/json' };
    return this._HttpClient.post(
      `${this.api}/user/Delete`,
      { id },
      { headers }
    );
  }

  GetUsersFromFile(file: any) {
    const headers = { 'Content-Type': 'application/json' };
    return this._HttpClient.post(
      `${this.api}/user/GenerateUsersFromFile`,
      { userFile: file },
      { headers }
    );
  }
}
