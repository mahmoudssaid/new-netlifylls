import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AddUserComponent } from './add-user/add-user.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SearchNamePipe } from './search-name.pipe';
import { ControlComponent } from './control/control.component';
import { GradingBookComponent } from './grading-book/grading-book.component';
import { LearningAnalysesComponent } from './learning-analyses/learning-analyses.component';
import { LabeSectionComponent } from './labe-section/labe-section.component';
import { ExperimentsComponent } from './experiments/experiments.component';
import { CoursesComponent } from './courses/courses.component';
import { PermissionComponent } from './permission/permission-table/permission.component';
import { RoleTableComponent } from './role/role-table/role-table.component';
import { EditPermissionComponent } from './permission/edit-permission/edit-permission.component';
import { HttpClientModule } from '@angular/common/http';
import { AddOrEditRoleComponent } from './role/add-or-edit-role/add-or-edit-role.component';
import { UserComponent } from './user/users-table/user.component';
import { AddOrEditUserComponent } from './user/add-or-edit-user/add-or-edit-user.component';

@NgModule({
  declarations: [
    AppComponent,
    AddUserComponent,
    SearchNamePipe,
    ControlComponent,
    GradingBookComponent,
    LearningAnalysesComponent,
    LabeSectionComponent,
    ExperimentsComponent,
    CoursesComponent,
    PermissionComponent,
    RoleTableComponent,
    EditPermissionComponent,
    AddOrEditRoleComponent,
    UserComponent,
    AddOrEditUserComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
