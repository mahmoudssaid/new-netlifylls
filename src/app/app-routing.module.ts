import { EditPermissionComponent } from './permission/edit-permission/edit-permission.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddUserComponent } from './add-user/add-user.component';
import { ControlComponent } from './control/control.component';
import { CoursesComponent } from './courses/courses.component';
import { ExperimentsComponent } from './experiments/experiments.component';
import { LabeSectionComponent } from './labe-section/labe-section.component';
import { LearningAnalysesComponent } from './learning-analyses/learning-analyses.component';
import { GradingBookComponent } from './grading-book/grading-book.component';
import { PermissionComponent } from './permission/permission-table/permission.component';
import { RoleTableComponent } from './role/role-table/role-table.component';
import { LoadingGuardGuard } from './loading-guard.guard';
import { AddOrEditRoleComponent } from './role/add-or-edit-role/add-or-edit-role.component';
import { UserComponent } from './user/users-table/user.component';
import { AddOrEditUserComponent } from './user/add-or-edit-user/add-or-edit-user.component';

const routes: Routes = [
  { path: '', redirectTo: 'control', pathMatch: 'full' },
  { path: 'Courses', component: CoursesComponent },
  { path: 'Experiments', component: ExperimentsComponent },
  { path: 'Labe-section', component: LabeSectionComponent },
  { path: 'learning-Analyses', component: LearningAnalysesComponent },
  { path: 'Grading-Book', component: GradingBookComponent },
  /* ---------------------------- control component --------------------------- */
  {
    path: 'control',
    component: ControlComponent,
    children: [
      { path: '', redirectTo: 'users', pathMatch: 'full' },
      /* ----------------------------- users component ---------------------------- */
      { path: 'users', component: UserComponent },
      {
        path: 'addUser',
        component: AddOrEditUserComponent,
        canDeactivate: [LoadingGuardGuard],
      },

      /* -------------------------- permission component -------------------------- */
      { path: 'permission', component: PermissionComponent },
      {
        path: 'editPermission/:id',
        component: EditPermissionComponent,
        canDeactivate: [LoadingGuardGuard],
      },

      /* ---------------------------------- roles component --------------------------------- */
      { path: 'role', component: RoleTableComponent },
      {
        path: 'AddRole',
        component: AddOrEditRoleComponent,
        canDeactivate: [LoadingGuardGuard],
      },
      {
        path: 'EditRole/:id',
        component: AddOrEditRoleComponent,
        canDeactivate: [LoadingGuardGuard],
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
