import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PermissionDataService } from '../../services/permission-data.service';

@Component({
  selector: 'app-permission',
  templateUrl: './permission.component.html',
  styleUrls: ['./permission.component.scss'],
})
export class PermissionComponent implements OnInit {
  constructor(
    private _Router: Router,
    private _PermissionDataService: PermissionDataService
  ) {}

  allPermission: any = [];

  ngOnInit(): void {
    this._PermissionDataService.getPermissions().subscribe((res) => {
      this.allPermission = res.data;
    });
  }
  editPermission() {
    // this._Router.navigate(['./control/editPermission']);
  }
}
