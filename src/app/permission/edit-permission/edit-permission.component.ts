import {
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { PermissionDataService } from 'src/app/services/permission-data.service';
import { ActivatedRoute, Router } from '@angular/router';
import { RolesDataService } from 'src/app/services/roles-data.service';

@Component({
  selector: 'app-edit-permission',
  templateUrl: './edit-permission.component.html',
  styleUrls: ['./edit-permission.component.scss'],
})
export class EditPermissionComponent implements OnInit {
  editPerForm!: FormGroup;
  Permission: any = [];
  roles: any[] = [];
  isID: string = '';

  constructor(
    private FB: FormBuilder,
    private _PermissionDataService: PermissionDataService,
    private _ActivatedRoute: ActivatedRoute,
    private _RolesDataService: RolesDataService,
    private _Router: Router
  ) {
    this.isID = _ActivatedRoute.snapshot.params.id;
    this.editPerForm = FB.group({
      id: [''],
      title: [
        '',
        [
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(15),
        ],
      ],
      description: [''],
      roles: this.FB.array([]),
    });
  }

  ngOnInit(): void {
    /* ------------------------------ get all roles ----------------------------- */
    this._RolesDataService.getRoles().subscribe((res) => {
      this.roles = res.data;
    });

    /* --------------------------- return data to form -------------------------- */
    this._PermissionDataService
      .getPermissionById(this.isID)
      .subscribe((res) => {
        this.editPerForm = this.FB.group({
          id: [res.data.id],
          title: [res.data.title],
          description: [res.data.description],
          roles: [res.data.roles],
        });
      });
  }
  /* ----------------------- to get value from check box ---------------------- */
  onChange(id: string, isChecked: any) {
    let isCheckedS = isChecked.checked;
    const roleForm = <FormArray>this.editPerForm.controls.roles;

    if (isCheckedS == true) {
      this.editPerForm.value.roles.push(id);
    } else {
      if (this.editPerForm.value.roles.indexOf(id) != -1) {
        let index = this.editPerForm.value.roles.indexOf(id);
        this.editPerForm.value.roles.splice(index, 1);
      }
    }
  }

  /* ------------------------------- Submit Form ------------------------------ */
  submitPermission(form: any) {
    this._PermissionDataService.editPermissions(form).subscribe((res) => {});
    this.editPerForm.reset();
    this._Router.navigate(['/control/permission']);
  }

  cancel() {
    this._Router.navigate(['/control/permission']);
  }

  deletePermission(form: any) {
    this._PermissionDataService
      .DeletePermissions(form.value)
      .subscribe((res) => {
        console.log(res);
      });
    this._Router.navigate(['/control/permission']);
  }
  /* ------------------------------ CanDeactivate ----------------------------- */
  CanDeactivate() {
    if (this.editPerForm.status == 'VALID' && this.editPerForm.touched) {
      return window.confirm('your data not save');
    } else {
      return true;
    }
  }
}
